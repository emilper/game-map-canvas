"use strict"

class UnitKnowledge {
    constructor(data) {
        this.units = new Map() // TODO don't use Map directly, already have lots of maps in this code, encapsulate 
        this.towns = new Map()
        this.land = new Map()
        this.resources = new Map()
    }
}

export { UnitKnowledge }
