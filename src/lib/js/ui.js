'use strict'

import { viewport_center } from "Viewport.js"
import { world_map_viewport_details, world_map_draw } from "motor.js"
import { GameState } from "GameState.js"
import { Pointer } from "Pointer.js"
import { start_game } from "motor.js"

function init_keyboard_shortcuts(my_store) {
    return {
        "ArrowUp":    () => my_store.selected().move(my_store, "N"),
        "ArrowDown":  () => my_store.selected().move(my_store, "S"),
        "ArrowLeft":  () => my_store.selected().move(my_store, "W"),
        "ArrowRight": () => my_store.selected().move(my_store, "E"),
        "PageUp":     () => my_store.selected().move(my_store, "NE"),
        "Home":       () => my_store.selected().move(my_store, "NW"),
        "PageDown":   () => my_store.selected().move(my_store, "SE"),
        "End":        () => my_store.selected().move(my_store, "SW"),

        "c": function () {
            viewport_center(
                my_store,
                my_store.units[my_store.selected_entity.id].x,
                my_store.units[my_store.selected_entity.id].y
            )
            window.redraw_all()
        }
    }
}

const saves_list_name = 'motte-and-bailey-saves-list'
function init_ui_button_actions(window, my_store, game_state) {

    window.game_load = (id) => game_load(my_store, id)
    window.game_save = (id) => game_save(my_store, id)
    window.game_start = () => game_start(my_store.game_state)
    window.game_stop = () => game_stop(my_store.game_state)
    window.game_pause = () => game_pause(my_store.game_state)
    window.game_reset = () => game_reset()
    window.saves_list = () => saves_list()
    window.zoom_out = () => zoom_out(my_store)
    window.zoom_in = () => zoom_in(my_store)
    window.next_turn = () => next_turn(my_store)
}

function zoom_in(my_store) {
    if (my_store.world_map_zoom >= 10) {
        return false;
    }

    my_store.world_map_zoom += 1
    console.log("zoom is " + my_store.world_map_zoom)
    world_map_draw(my_store)
}

function zoom_out(my_store) {
    if (my_store.world_map_zoom < 2) {
        return false;
    }
    my_store.world_map_zoom -= 1
    console.log("zoom is " + my_store.world_map_zoom)
    world_map_draw(my_store)
}

function next_turn(my_store) {
    window.redraw_all()
}

function saves_list() {
    const saves_list = JSON.parse(localStorage.getItem(saves_list_name))
    ui_msg(saves_list)
}

function game_load(my_store, id) {
    const saved_store = JSON.parse(localStorage.getItem("save_game_" + String(id)));
    ui_msg("loading " + 1)
    const new_store = new GameState(saved_store)
    const selected_entity_type = new_store.selected_entity.type
    const selected_entity = new_store[selected_entity_type][new_store.selected_entity.id]
    console.log(new_store.selected_entity)
    start_game(new_store)
}

function game_save(my_store, id) {
    ui_msg(my_store)
    const save_id = "save_game_" + String(id)
    localStorage.setItem(save_id, JSON.stringify(my_store))

     const saves_list = JSON.parse(localStorage.getItem(saves_list_name));
    if ( Array.isArray(saves_list) ) {
        const temp_list = new Array();
        const uniques =
            [ ...saves_list, save_id]
                .filter( function(item) {
                    if ( temp_list.indexOf(item) == -1 ) {
                        temp_list.push(item)
                        return true
                    }
                    return false
                })

        ui_msg('+++', saves_list, uniques)

        localStorage.setItem(saves_list_name, JSON.stringify(uniques))
    } else {
        localStorage.setItem(saves_list_name, JSON.stringify([save_id]))
    }
}

function game_start(game_state) {
    ui_msg("starting game")
    game_state.paused = false
}

function game_stop() {
    ui_msg("quitting game")
}

function game_pause(game_state) {
    ui_msg("pausing game")
    game_state.paused = true
}

function game_reset() {
    ui_msg("resetting game")
}

function build_entities_list(
    container_id,
    store,
    type,
    selected_entity_id
    ) {

    // remove all children and event listeners
    const old_container = document.getElementById(container_id)
    if (!old_container) {
        throw "could not find the unit list container '"+ container_id + "'"
    }
    const container = old_container.cloneNode()
    old_container.parentNode.replaceChild(container, old_container)


    const entities = store[type]
    entities.forEach(
        function(entity) {
            if (
                entity.is_own()
                || (
                    entity.is_landmark()
                    && entity.is_known()
                    )
                ) {
                container.appendChild(
                    build_entity_list_item(
                        store,
                        type,
                        entity,
                        selected_entity_id
                    )
                )
            }
        }
    )
}

function build_entity_list_item(store, entity_type, entity, selected_entity_id) {
    const el_container = document.createElement("div")
    el_container.classList.add(entity_type + "_selector_container")

    const el = document.createElement("div")
    el.id = entity_type + "_id__" + entity.id
    el.classList.add('unit_selector')
    if (
            entity.id == selected_entity_id
            && entity.unit_type == store.selected_entity.type
        ) {
        el.classList.add('selected_' + entity_type + '_selector')
    }
    el.innerHTML = entity.icon;

    el.onclick = function () {
        if (store.pointer !== null ) {
            store.pointer = null
        }

        remove_selection_mark_from_previously_selected_unit(store)
        store.selected_entity.id = entity.id
        store.selected_entity.type = entity_type

        document.getElementById(entity_type + "_id__" + entity.id)
            .classList.add("selected_" + entity_type + "_selector")

        viewport_center(store, entity.x, entity.y)
        window.redraw_all()
    }

    el_container.appendChild(el)

    const el_details = document.createElement("div")
    el_details.classList.add(entity_type + "_selector_details")
    el_details.innerHTML = entity.id + ": " + entity.x + " " + entity.y
    el_container.appendChild(el_details)
    return el_container
}

function remove_selection_mark_from_previously_selected_unit(store) {
    const old_selected_entity_id = store.selected_entity.id
    const old_selected_entity_type = store.selected_entity.type

    const selected_entity_doc_id = old_selected_entity_type + "_id__" + old_selected_entity_id;
    if (document.getElementById(selected_entity_doc_id)) {
        document.getElementById(selected_entity_doc_id)
            .classList
            .remove('selected_' + old_selected_entity_type + '_selector')
    }
}

function keyboard_actions(my_store) {

    const move_keys = init_keyboard_shortcuts(my_store);
    return function (e) {
        if (my_store.game_state.paused === true ) {
            return false
        }

        if (e.key in move_keys) {
            move_keys[e.key]()
            e.preventDefault()
            e.stopPropagation()
        }
    }
}

function init_world_map_events(store, world_map_canvas_id) {

    const old_canvas = document.getElementById("world_map_canvas")
    // SEEME TODO cheap and dirty solution
    // replacing the canvas with a clone to remove all event listeners
    // when reloading the game; a better solution is needed TODO such as
    // making a list of event listeners and removing them
    const canvas = old_canvas.cloneNode()
    old_canvas.parentNode.replaceChild(canvas, old_canvas)
    canvas.addEventListener(
        'mousedown',
        function(e) {
            const {
                center,
                offset,
                viewport,
                zoom } = world_map_viewport_details( store )

            const x = (e.pageX - canvas.offsetLeft) / store.world_map_zoom
            const y = (e.pageY - canvas.offsetTop)  / store.world_map_zoom
            ui_msg(
                "centering on: ",
                Math.floor(x) + offset.x,
                Math.floor(y) + offset.y
            )


            store.pointer = new Pointer(
                Math.floor(x) + offset.x,
                Math.floor(y) + offset.y
            )
            console.log("=================")
            console.log(store.pointer)
            console.log("=================")
            viewport_center(
                store,
                store.pointer.x,
                store.pointer.y
            )
            window.redraw_all()
            e.preventDefault()
            e.stopPropagation()
        },
        false
    );
}

function make_sprites() {
    const fort = new Image()
    fort.src = 'sprites/fort_topdown.png'

    const unit = new Image()
    unit.src = 'sprites/unit_topdown.png'

    const shield = new Image(64,64)
    shield.src = 'sprites/blank_shield.png'

    const cart = new Image(64,64)
    cart.src = 'sprites/small_cart.png'

    const fog_of_war = new Image()
    fog_of_war.src = 'sprites/fog_of_war.png'

    return {
        "fog_of_war": fog_of_war,
        "town": fort,
        "unit": unit,
        "shield": shield,
        "cart": cart
    }
}

function init_tiles() {

    //TODO: use a canvas element to combine image and create the final tiles
    // https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Pixel_manipulation_with_canvas

    const tiles = new Array()
    tiles[1] = new Array()
    tiles[1][0] = new Image()
    tiles[1][0].src = "sprites/swamp_1_dark.png"
    tiles[1][1] = new Image()
    tiles[1][1].src = "sprites/swamp_1.png"

    tiles[2] = new Array()
    tiles[2][0] = new Image()
    tiles[2][0].src = "sprites/swamp_2_dark.png"
    tiles[2][1] = new Image()
    tiles[2][1].src = "sprites/swamp_2.png"

    tiles[3] = new Array()
    tiles[3][0] = new Image()
    tiles[3][0].src = "sprites/swamp_3_dark.png"
    tiles[3][1] = new Image()
    tiles[3][1].src = "sprites/swamp_3.png"

    tiles[4] = new Array()
    tiles[4][0] = new Image()
    tiles[4][0].src = "sprites/forest_1_dark.png"
    tiles[4][1] = new Image()
    tiles[4][1].src = "sprites/forest_1.png"

    tiles[5] = new Array()
    tiles[5][0] = new Image()
    tiles[5][0].src = "sprites/forest_2_dark.png"
    tiles[5][1] = new Image()
    tiles[5][1].src = "sprites/forest_2.png"

    tiles[6] = new Array()
    tiles[6][0] = new Image()
    tiles[6][0].src = "sprites/forest_3_dark.png"
    tiles[6][1] = new Image()
    tiles[6][1].src = "sprites/forest_3.png"

    tiles[7] = new Array()
    tiles[7][0] = new Image()
    tiles[7][0].src = "sprites/lowlands_forest_topdown_dark.png"
    tiles[7][1] = new Image()
    tiles[7][1].src = "sprites/lowlands_forest_topdown.png"

    tiles[8] = new Array()
    tiles[8][0] = new Image()
    tiles[8][0].src = "sprites/lowlands_forest_topdown_tileable_dark.png"
    tiles[8][1] = new Image()
    tiles[8][1].src = "sprites/lowlands_forest_topdown_tileable.png"

    return tiles
}

export {
    init_tiles,
    make_sprites,
    init_world_map_events,
    keyboard_actions,
    init_keyboard_shortcuts,
    init_ui_button_actions,
    build_entities_list,
    remove_selection_mark_from_previously_selected_unit
}
