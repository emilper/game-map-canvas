'use strict';

const GameState = require('GameState.js').GameState

module.exports.store = new GameState()

// module.exports.set_state = function(new_state) {
//     module.exports.store = new_state
// }

module.exports.debug_info = {
    "show": true
}

module.exports.map_palette = []
module.exports.map_palette_dark = []
