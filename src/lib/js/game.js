'use strict';

import {
    start_game,
    game_tick,
    world_map_draw,
    world_map_viewport_details
} from "motor.js"
import { Map } from 'Map.js'
import { Unit } from 'Unit.js'
import { Town } from 'Town.js'
import { Pointer } from 'Pointer.js'
import {
    map_data,
    tile_is_walkable
} from 'mock_server_data.js'
import {
    init_keyboard_shortcuts,
    init_ui_button_actions,
    build_entities_list,
    remove_selection_mark_from_previously_selected_unit
} from 'ui.js'
import * as ui from "ui.js"
import globals from 'globals.js'

import utils from 'misc_not_mine.js'
import { now_formatted } from 'util.js'
import { viewport_center } from 'Viewport.js'

require("mini.css/dist/mini-nord.css")
require("Style/main.css")

const map_palette_init = [
    [ 23,  87, 126], //"#17577e"
    [ 61, 108,  66], //"#3d6c42"
    [ 63, 110,  66], //"#3f6e42"
    [ 71, 115,  64], //"#477340"
    [ 82, 123,  62], //"#527b3e"
    [ 97, 133,  59], //"#61853b"
    [114, 144,  56], //"#729038"
    [143, 164,  51], //"#8fa433"
    [175, 186,  45], //"#afba2d"
    [184, 192,  43], //"#b8c02b"
    [169, 166,  42], //"#a9a62a"
    [141, 115,  41], //"#8d7329"
    [117,  71,  39], //"#754727"
    [107,  53,  39], //"#6b3527"
    [131,  86,  74], //"#83564a"
    [195, 173, 167]  //"#c3ada7"
]


globals.map_palette = map_palette_init.map( rgb_value => rgb_to_string(rgb_value) )
globals.map_palette_dark = map_palette_init.map( rgb_value => rgb_to_string(rgb_value, -30) )
let store = globals.store
store.tiles = ui.init_tiles()
store.sprites = ui.make_sprites()
store.units = init_units(store)
store.towns = init_towns(store)

window.onload = () => start_game(store)




function init_towns(my_store) {
    // interesting places
    // 712 170
    // 709 1142
    // 709 1142
    // 1202 1292
    // even better 495 1268 ?
    // best 134 876

    const towns = Array()
    towns[0] = new Town(0, 132, 877)
    towns[0].is_known(true)
    towns[1] = new Town(1, 137, 871)
    return towns;
}

function init_units(my_store) {

    const units = Array(10)
    // mock some units
    let unit_id = 0;
    let count = 0;
    while ( unit_id < units.length ) {
        const x = 120 + utils.get_random_int(30)
        const y = 860 + utils.get_random_int(30)

        // only generate unit if in the low lands
        const map_check = tile_is_walkable(map_data, x, y)
        if ( map_check.success == true) {
            units[unit_id] = new Unit(unit_id, x, y)
            unit_id += 1
        }
        count++
        if (count > 400) {
            break
        }
    }

    // make some friendlies
    units[0].is_friendly(true)
    units[0].is_own(true)
    units[1].is_friendly(true)
    units[1].is_own(false)
    units[2].is_friendly(true)
    units[2].is_own(false)
    return units;
}


window.ui_msg = function (...args) {
    const console_el = document.getElementById('console_area')
    console_el.innerHTML =
        "<div class='messages_item'>"
        + now_formatted()
        + " "
        + args.join(" ")
        + '</div>'
        + console_el.innerHTML
}

function rgb_to_string(rgb, change) {
    if (
        Number.isInteger(change)
        && change >= -255
        && change <= 255
    ) {
        rgb = rgb.map( value => value + change >= 0 ? value + change : 0)
    }
    return "rgb(" + rgb.join(",") + ")"
}
